const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const style = require('./styles');
const html2html = require('./html2html');
const svgsprite = require('./svgspite');

module.exports = function serve(cb){
    browserSync.init({
        server: 'build',
        notify: false,
        open: true,
        cors: true
    });
    gulp.watch('src/svg/*.svg'),gulp.series(svgsprite)
    gulp.watch('build/sprites/*.svg').on('change', browserSync.reload)
    gulp.watch("src/styles/**/*.scss", gulp.series(style,cb=>gulp.src('build/css').pipe(browserSync.stream()).on('end', cb)))
    gulp.watch("src/**/*.html", gulp.series(html2html))
    gulp.watch('build/*.html').on('change', browserSync.reload)
    return cb()
};