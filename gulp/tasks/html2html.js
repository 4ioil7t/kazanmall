const gulp = require('gulp');
const gulpHtmlBemValidator = require('gulp-html-bem-validator');

module.exports = function styles(){
    return gulp.src('src/**/*.html')
        .pipe(gulpHtmlBemValidator())
        .pipe(gulp.dest('build/'))
}