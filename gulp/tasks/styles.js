const gulp = require('gulp');
const sass = require('gulp-sass');
const gcmq = require('gulp-group-css-media-queries');

module.exports = function styles(){
    return gulp.src('src/styles/*.scss')
        .pipe(sass())
        .pipe(gcmq())
        .pipe(gulp.dest('build/css'))
}