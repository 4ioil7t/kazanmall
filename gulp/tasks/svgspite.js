const gulp = require('gulp');
const dfg = require('gulp-svg-sprite');

let config = {
    shape: {
    dimension: { // Set maximum dimensions
        maxWidth: 32,
        maxHeight:32
    },
    dest: 'out/intermediate-svg' // Keep the intermediate files
       },
    mode: {
    view: { // Activate the «view» mode
    bust: false,
    render: {
    scss: true // Activate Sass output (with default options)
           }
         },
    symbol: true
       }
     };

module.exports = function svgSprite(){
    return gulp.src('src/svg/*.svg')
        .pipe(dfg(config))
        .pipe(gulp.dest('build/'));
};