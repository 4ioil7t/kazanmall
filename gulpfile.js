const gulp = require('gulp');
const style = require('./gulp/tasks/styles');
const serve = require('./gulp/tasks/serve');
const svgsprite = require('./gulp/tasks/svgspite'); 

module.exports.start = gulp.series(svgsprite,style,serve)