/**
 * Находится ли основной навбар в зоне видимости
 */
function isHeaderOnScreen() {
    const header = document.getElementsByClassName('nav-menu')[0];
    const rect = header.getBoundingClientRect();
    const tapok = document.getElementsByClassName('static-menu')[0];

    return (0 <= rect.bottom - tapok.getBoundingClientRect().height - 30);
}

/**
 * Установить видимость статичного навбара
 * @param {boolean} visibility
 */
function setStaticNavVisibility(visibility) {
    const tapok = document.getElementsByClassName('static-menu')[0];
    const header = document.getElementsByClassName('nav-menu')[0];
    header.style.visibility = !visibility ? 'visible' : 'hidden';
    tapok.style.visibility = visibility ? 'visible' : 'hidden';
}

document.addEventListener('scroll', () => {
    setStaticNavVisibility(!isHeaderOnScreen());
});