const paralax = document.getElementsByClassName('paralax')[0];
document.addEventListener('mousemove',(e) => {
    if(e.movementX<50 && e.movementY<50){
        paralax.style.top =`${paralax.style.top.split('px')[0] - e.movementY * 0.01}px`;
        paralax.style.left = `${paralax.style.left.split('px')[0] - e.movementX * 0.01}px`;
    }
    
})